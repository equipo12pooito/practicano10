package clases.ito.poo;

import excepciones.ito.poo.excepcionID;

public interface Arreglo <T>{
	
	public boolean addItem(T item) throws excepcionID;

	public boolean existeItem(T item);
	
	public T getItem(int pos);

	public int getSize();

	public boolean clear(T item);

	public boolean isFree();

	public boolean isFull();

}
