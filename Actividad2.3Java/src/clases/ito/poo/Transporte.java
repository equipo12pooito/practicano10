package clases.ito.poo;

import java.time.LocalDate;
import java.util.ArrayList;
import excepciones.ito.poo.excepcionFechas;

public class Transporte implements Comparable <Transporte>{
	
	private boolean verifFechasAdd(Viaje v) throws excepcionFechas{
		boolean aux = true;
		if (v.getFechaViaje().compareTo(this.fechaAdquisicion) < 0) {
			aux = false;
			throw new excepcionFechas("ERROR: La fecha de salida no puede ser menor o igual a la fecha de adquisicion de la unidad");
		} else if (v.getFechaRegreso().compareTo(v.getFechaViaje()) < 0) {
			aux = false;
			throw new excepcionFechas("ERROR: La fecha de regreso debe ser mayor o igual a la fecha de salida");
		} else if (verifFechasOcupadas(v) == true) {
			aux = false;
		} else
			this.viajesRealizados.add(v);
		return aux;
	}
	
	private boolean verifFechasOcupadas(Viaje v){
		boolean aux = false;
		for (Viaje v2 : this.viajesRealizados) 
			if (v2.getFechaViaje().compareTo(v.getFechaViaje()) == 0 || v2.getFechaRegreso().compareTo(v.getFechaRegreso()) == 0)
				aux = true;
		return aux;
	}
	
	//========================================
	
	private int identificador;
	private String marca = "";
	private String modelo = "";
	private float capacidadMaxima = 0F;
	private LocalDate fechaAdquisicion = null;
	private ArrayList<Viaje> viajesRealizados = new ArrayList<Viaje>();

	public Transporte() {
		super();
	}

	public Transporte(int identificador, String marca, String modelo, float capacidadMaxima, LocalDate fechaAdquisicion) {
		super();
		this.identificador = identificador;
		this.marca = marca;
		this.modelo = modelo;
		this.capacidadMaxima = capacidadMaxima;
		this.fechaAdquisicion = fechaAdquisicion;
	}

	//========================================
	
	public int getIdentificador() {
		return this.identificador;
	}
	
	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String newMarca) {
		this.marca = newMarca;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String newModelo) {
		this.modelo = newModelo;
	}

	public float getCapacidadMaxima() {
		return this.capacidadMaxima;
	}

	public void setCapacidadMaxima(float newCapacidadMaxima) {
		this.capacidadMaxima = newCapacidadMaxima;
	}

	public LocalDate getFechaAdquisicion() {
		return this.fechaAdquisicion;
	}

	public void setFechaAdquisicion(LocalDate newFechaAdquisicion) {
		this.fechaAdquisicion = newFechaAdquisicion;
	}

	
	public ArrayList<Viaje> getViajesRealizados() {
		return this.viajesRealizados;
	}
	
	public Viaje getViaje(int i) {
		Viaje o;
		if (i > viajesRealizados.size() || i < 0)
			o = new Viaje();
		else o = this.getViajesRealizados().get(i);
		return o;
	}

	//========================================
	

	public boolean addViaje(Viaje newViaje) throws excepcionFechas {
		return verifFechasAdd(newViaje);
	}

	public boolean elimViaje(Viaje viaje) {
		boolean elimViaje = false;
		elimViaje = this.viajesRealizados.remove(viaje);
		return elimViaje;
	}
	
	
	//========================================
	
	@Override
	public String toString() {
		return "ID: " + identificador+ ", Transporte " + marca + " " + modelo + " \nCapacidad Maxima: " + capacidadMaxima + " TON"
				+ "\nFecha Adquisicion: " + fechaAdquisicion;
	}

	@Override
	public int compareTo(Transporte o) {
		if (identificador < o.getIdentificador())
			return -1;
		else if (identificador > o.getIdentificador())
			return 1;
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		Transporte o = (Transporte) obj;
		if (identificador == o.getIdentificador())
			return true;
		if (marca == o.getMarca())
			if (modelo == o.getModelo())
				if (fechaAdquisicion.equals(o.getFechaAdquisicion()))
					return true;
		return false;
	}
	
}
