package clases.ito.poo;

import java.time.LocalDate;

public class Viaje implements Comparable <Viaje> {

	private String destino = "";
	private String direccion = "";
	private LocalDate fechaViaje = null;
	private LocalDate fechaRegreso = null;
	private String descripcionCarga = "";
	private float monto = 0F;

	public Viaje() {
		super();
	}

	public Viaje(String destino, String direccion, LocalDate fechaViaje, LocalDate fechaRegreso,
			String descripcionCarga, float monto) {
		super();
		this.destino = destino;
		this.direccion = direccion;
		this.fechaViaje = fechaViaje;
		this.fechaRegreso = fechaRegreso;
		this.descripcionCarga = descripcionCarga;
		this.monto = monto;
	}

	//========================================
	
	public String getDestino() {
		return this.destino;
	}

	public void setDestino(String newDestino) {
		this.destino = newDestino;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String newDireccion) {
		this.direccion = newDireccion;
	}

	public LocalDate getFechaViaje() {
		return this.fechaViaje;
	}

	public void setFechaViaje(LocalDate newFechaViaje) {
		this.fechaViaje = newFechaViaje;
	}

	public LocalDate getFechaRegreso() {
		return this.fechaRegreso;
	}

	public void setFechaRegreso(LocalDate newFechaRegreso) {
		this.fechaRegreso = newFechaRegreso;
	}

	public String getDescripcionCarga() {
		return this.descripcionCarga;
	}

	public void setDescripcionCarga(String newDescripcionCarga) {
		this.descripcionCarga = newDescripcionCarga;
	}

	public float getMonto() {
		return this.monto;
	}
	
	public void setMonto(float newMonto) {
		this.monto = newMonto;
	}
	
	//========================================

	@Override
	public String toString() {
		return "Destino: " + destino + "\nDireccion: " + direccion + "\nFecha inicio de Viaje: " + fechaViaje
				+ ", Fecha regreo de Viaje: " + fechaRegreso + "\nCarga: " + descripcionCarga + "\nMonto: " 
				+ monto;
	}

	@Override
	public int compareTo(Viaje o) {
		if (fechaViaje.compareTo(o.getFechaViaje()) < 0)
			return -1;
		if (fechaViaje.compareTo(o.getFechaViaje()) > 0)
			return 1;
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		Viaje o = (Viaje) obj;
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (fechaViaje == o.getFechaViaje())
			if (fechaRegreso == o.getFechaRegreso())
				if (descripcionCarga == o.getDescripcionCarga())
					return true;
		return false;
	}

}
